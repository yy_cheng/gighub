﻿namespace GigHub.Dtos
{
    public class FollowingArtistDto
    {
        public string ArtistId { get; set; }
    }
}
