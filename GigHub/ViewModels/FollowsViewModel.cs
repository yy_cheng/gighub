﻿using GigHub.Models;
using System.Collections.Generic;

namespace GigHub.ViewModels
{
    public class FollowsViewModel
    {
        public List<ApplicationUser> Follows { get; set; }
        public bool ShowAction { get; set; }
    }
}