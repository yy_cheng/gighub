﻿using GigHub.Models;
using System.Collections.Generic;

namespace GigHub.ViewModels
{
    public class GigsViewModel
    {
        public bool ShowAction { get; set; }
        public IEnumerable<Gig> UpcomingGigs { get; set; }
    }
}