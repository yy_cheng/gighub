namespace GigHub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class followerAndFolloweeInApplicationUser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Follows", "FolloweeId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Follows", "FollowerId", "dbo.AspNetUsers");
            AddForeignKey("dbo.Follows", "FolloweeId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Follows", "FollowerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Follows", "FollowerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Follows", "FolloweeId", "dbo.AspNetUsers");
            AddForeignKey("dbo.Follows", "FollowerId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Follows", "FolloweeId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
