﻿using GigHub.Dtos;
using GigHub.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Http;

namespace GigHub.Controllers
{
    [Authorize]
    public class FollowArtistController : ApiController
    {
        public ApplicationDbContext _context;

        public FollowArtistController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        [Route("api/follows")]
        public IHttpActionResult Follow(FollowingArtistDto dto)
        {
            var userId = User.Identity.GetUserId();
            var exists = _context.Follows.Any(f => f.FolloweeId == userId && f.FollowerId == dto.ArtistId);
            if (exists)
            {
                return BadRequest();
            }

            var follow = new Follow()
            {
                FolloweeId = dto.ArtistId,
                FollowerId = userId
            };
            _context.Follows.Add(follow);
            _context.SaveChanges();

            return Ok();
        }
    }
}
